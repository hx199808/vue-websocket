var express = require("express");
var expressWs = require("express-ws");
var app = express();
expressWs(app);  //将 express 实例上绑定 websock 的一些方法
app.ws("/socketTest", function (ws, req) {
    ws.send("你连接成功了");
    // ws.on("message", function (msg) {
    //     ws.send("这是第二次发送信息");
    // });
    setInterval(()=>{
        ws.send("发送消息");
    }, 1000)
});
app.listen(3000);
console.log("Listening on port 3000...");
